<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html
  PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
   <head>
      <title>PMD 5.5.0 Report</title>
      <style type="text/css">
    .bannercell {
      border: 0px;
      padding: 0px;
    }
    body {
      margin-left: 10px;
      margin-right: 10px;
      font:normal 80% arial,helvetica,sanserif;
      background-color:#FFFFFF;
      color:#000000;
    }
    .a td {
      background: #efefef;
    }
    .b td {
      background: #fff;
    }
    th, td {
      text-align: left;
      vertical-align: top;
    }
    th {
      font-weight:bold;
      background: #ccc;
      color: black;
    }
    table, th, td {
      font-size:100%;
      border: none
    }
    table.log tr td, tr th {

    }
    h2 {
      font-weight:bold;
      font-size:140%;
      margin-bottom: 5;
    }
    h3 {
      font-size:100%;
      font-weight:bold;
      background: #525D76;
      color: white;
      text-decoration: none;
      padding: 5px;
      margin-right: 2px;
      margin-left: 2px;
      margin-bottom: 0px;
    }
	.p1 { background:#FF9999; }
	.p2 { background:#FFCC66; }
	.p3 { background:#FFFF99; }
	.p4 { background:#99FF99; }
	.p5 { background:#a6caf0; }

		</style>
   </head>
   <body>
      <a name="top"/>
      <table border="0" cellpadding="0" cellspacing="0" width="100%">
         <tr>
            <td class="bannercell" rowspan="2"/>
            <td class="text-align:right">
               <h2>PMD 5.5.0 Report. Generated on 2016-08-23 - 05:44:26</h2>
            </td>
         </tr>
      </table>
      <hr size="1"/>
      <h3>Summary</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:25%">Files</th>
            <th>Total</th>
            <th>
               <div class="p1">Priority 1</div>
            </th>
            <th>
               <div class="p2">Priority 2</div>
            </th>
            <th>
               <div class="p3">Priority 3</div>
            </th>
            <th>
               <div class="p4">Priority 4</div>
            </th>
            <th>
               <div class="p5">Priority 5</div>
            </th>
         </tr>
         <tr class="a">
            <td>53</td>
            <td>1313</td>
            <td>1</td>
            <td>2</td>
            <td>1283</td>
            <td>10</td>
            <td>17</td>
         </tr>
      </table>
      <hr size="1" width="100%" align="left"/>
      <h3>Rules</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:84%">Rule</th>
            <th style="width:8%">Violations</th>
            <th style="width:8%">Severity</th>
         </tr>
         <tr class="a">
            <td>
					[Coupling] LawOfDemeter</td>
            <td>502</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Comments] CommentRequired</td>
            <td>243</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Optimization] MethodArgumentCouldBeFinal</td>
            <td>126</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Comments] CommentSize</td>
            <td>124</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Optimization] LocalVariableCouldBeFinal</td>
            <td>87</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[JavaBeans] BeanMembersShouldSerialize</td>
            <td>43</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Controversial] AtLeastOneConstructor</td>
            <td>31</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Naming] ShortVariable</td>
            <td>23</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[String and StringBuffer] AvoidDuplicateLiterals</td>
            <td>18</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Controversial] DataflowAnomalyAnalysis</td>
            <td>17</td>
            <td>
               <div class="p5"> 5</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Design] ImmutableField</td>
            <td>13</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Naming] LongVariable</td>
            <td>11</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Controversial] OnlyOneReturn</td>
            <td>11</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[JUnit] JUnitTestContainsTooManyAsserts</td>
            <td>10</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Import Statements] UnusedImports</td>
            <td>7</td>
            <td>
               <div class="p4"> 4</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Controversial] UseConcurrentHashMap</td>
            <td>6</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[JUnit] JUnitSpelling</td>
            <td>6</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Naming] ShortClassName</td>
            <td>3</td>
            <td>
               <div class="p4"> 4</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Import Statements] TooManyStaticImports</td>
            <td>3</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Design] AvoidReassigningParameters</td>
            <td>2</td>
            <td>
               <div class="p2"> 2</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Design] UseLocaleWithCaseConversions</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Design] PreserveStackTrace</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Unused Code] UnusedModifier</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Optimization] RedundantFieldInitializer</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Braces] IfElseStmtsMustUseBraces</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Design] AbstractClassWithoutAbstractMethod</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Code Size] TooManyMethods</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Strict Exceptions] SignatureDeclareThrowsException</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Design] NonStaticInitializer</td>
            <td>2</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Naming] AvoidFieldNameMatchingTypeName</td>
            <td>1</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Controversial] CallSuperInConstructor</td>
            <td>1</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Unused Code] UnusedFormalParameter</td>
            <td>1</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Unused Code] UnusedPrivateField</td>
            <td>1</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Design] SingularField</td>
            <td>1</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Optimization] AvoidInstantiatingObjectsInLoops</td>
            <td>1</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Naming] AbstractNaming</td>
            <td>1</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
         <tr class="a">
            <td>
					[Strict Exceptions] AvoidThrowingRawExceptionTypes</td>
            <td>1</td>
            <td>
               <div class="p1"> 1</div>
            </td>
         </tr>
         <tr class="b">
            <td>
					[Controversial] AvoidLiteralsInIfCondition</td>
            <td>1</td>
            <td>
               <div class="p3"> 3</div>
            </td>
         </tr>
      </table>
      <hr size="1" width="100%" align="left"/>
      <h3>Files</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th>File</th>
            <th style="width:40px">
               <div class="p5">5</div>
            </th>
            <th style="width:40px">
               <div class="p4">4</div>
            </th>
            <th style="width:40px">
               <div class="p3">3</div>
            </th>
            <th style="width:40px">
               <div class="p2">2</div>
            </th>
            <th style="width:40px">
               <div class="p1">1</div>
            </th>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_web_OwnerControllerTests.java">src/test/java/org/springframework/samples/petclinic/web/OwnerControllerTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>194</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_service_AbstractClinicServiceTests.java">src/test/java/org/springframework/samples/petclinic/service/AbstractClinicServiceTests.java</a>
            </td>
            <td>0</td>
            <td>1</td>
            <td>175</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_web_PetControllerTests.java">src/test/java/org/springframework/samples/petclinic/web/PetControllerTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>102</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_web_VisitControllerTests.java">src/test/java/org/springframework/samples/petclinic/web/VisitControllerTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>50</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcOwnerRepositoryImpl.java">src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcOwnerRepositoryImpl.java</a>
            </td>
            <td>4</td>
            <td>0</td>
            <td>42</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcPetRepositoryImpl.java">src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcPetRepositoryImpl.java</a>
            </td>
            <td>1</td>
            <td>1</td>
            <td>41</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_web_OwnerController.java">src/main/java/org/springframework/samples/petclinic/web/OwnerController.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>42</td>
            <td>1</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_web_VetControllerTests.java">src/test/java/org/springframework/samples/petclinic/web/VetControllerTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>43</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_Owner.java">src/main/java/org/springframework/samples/petclinic/model/Owner.java</a>
            </td>
            <td>2</td>
            <td>0</td>
            <td>35</td>
            <td>1</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_service_ClinicServiceImpl.java">src/main/java/org/springframework/samples/petclinic/service/ClinicServiceImpl.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>36</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_web_PetController.java">src/main/java/org/springframework/samples/petclinic/web/PetController.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>35</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_PetclinicInitializer.java">src/main/java/org/springframework/samples/petclinic/PetclinicInitializer.java</a>
            </td>
            <td>0</td>
            <td>2</td>
            <td>30</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcVisitRepositoryImpl.java">src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcVisitRepositoryImpl.java</a>
            </td>
            <td>2</td>
            <td>1</td>
            <td>25</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_util_CallMonitoringAspect.java">src/main/java/org/springframework/samples/petclinic/util/CallMonitoringAspect.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>27</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_web_VisitController.java">src/main/java/org/springframework/samples/petclinic/web/VisitController.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>27</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_Pet.java">src/main/java/org/springframework/samples/petclinic/model/Pet.java</a>
            </td>
            <td>0</td>
            <td>2</td>
            <td>24</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcVetRepositoryImpl.java">src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcVetRepositoryImpl.java</a>
            </td>
            <td>6</td>
            <td>0</td>
            <td>18</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jpa_JpaOwnerRepositoryImpl.java">src/main/java/org/springframework/samples/petclinic/repository/jpa/JpaOwnerRepositoryImpl.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>23</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_web_CrashControllerTests.java">src/test/java/org/springframework/samples/petclinic/web/CrashControllerTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>23</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_model_ValidatorTests.java">src/test/java/org/springframework/samples/petclinic/model/ValidatorTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>22</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_web_PetTypeFormatter.java">src/main/java/org/springframework/samples/petclinic/web/PetTypeFormatter.java</a>
            </td>
            <td>1</td>
            <td>0</td>
            <td>20</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_web_PetTypeFormatterTests.java">src/test/java/org/springframework/samples/petclinic/web/PetTypeFormatterTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>19</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_web_VetController.java">src/main/java/org/springframework/samples/petclinic/web/VetController.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>18</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_web_PetValidator.java">src/main/java/org/springframework/samples/petclinic/web/PetValidator.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>16</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_Vet.java">src/main/java/org/springframework/samples/petclinic/model/Vet.java</a>
            </td>
            <td>0</td>
            <td>1</td>
            <td>14</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jpa_JpaVisitRepositoryImpl.java">src/main/java/org/springframework/samples/petclinic/repository/jpa/JpaVisitRepositoryImpl.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>13</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcPetVisitExtractor.java">src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcPetVisitExtractor.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>12</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jpa_JpaPetRepositoryImpl.java">src/main/java/org/springframework/samples/petclinic/repository/jpa/JpaPetRepositoryImpl.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>12</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_service_ClinicService.java">src/main/java/org/springframework/samples/petclinic/service/ClinicService.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>12</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_util_EntityUtils.java">src/main/java/org/springframework/samples/petclinic/util/EntityUtils.java</a>
            </td>
            <td>1</td>
            <td>0</td>
            <td>11</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcPet.java">src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcPet.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>11</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_BaseEntity.java">src/main/java/org/springframework/samples/petclinic/model/BaseEntity.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>10</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_Person.java">src/main/java/org/springframework/samples/petclinic/model/Person.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>10</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcPetRowMapper.java">src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcPetRowMapper.java</a>
            </td>
            <td>0</td>
            <td>1</td>
            <td>9</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcVisitRowMapper.java">src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcVisitRowMapper.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>9</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_NamedEntity.java">src/main/java/org/springframework/samples/petclinic/model/NamedEntity.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>8</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_Vets.java">src/main/java/org/springframework/samples/petclinic/model/Vets.java</a>
            </td>
            <td>0</td>
            <td>1</td>
            <td>7</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_OwnerRepository.java">src/main/java/org/springframework/samples/petclinic/repository/OwnerRepository.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>7</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_jpa_JpaVetRepositoryImpl.java">src/main/java/org/springframework/samples/petclinic/repository/jpa/JpaVetRepositoryImpl.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>7</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_springdatajpa_SpringDataOwnerRepository.java">src/main/java/org/springframework/samples/petclinic/repository/springdatajpa/SpringDataOwnerRepository.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>6</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_Visit.java">src/main/java/org/springframework/samples/petclinic/model/Visit.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>5</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_PetRepository.java">src/main/java/org/springframework/samples/petclinic/repository/PetRepository.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>5</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_VisitRepository.java">src/main/java/org/springframework/samples/petclinic/repository/VisitRepository.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>5</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_web_CrashController.java">src/main/java/org/springframework/samples/petclinic/web/CrashController.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>4</td>
            <td>0</td>
            <td>1</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_VetRepository.java">src/main/java/org/springframework/samples/petclinic/repository/VetRepository.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>4</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_service_ClinicServiceJdbcTests.java">src/test/java/org/springframework/samples/petclinic/service/ClinicServiceJdbcTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>3</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_PetType.java">src/main/java/org/springframework/samples/petclinic/model/PetType.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_model_Specialty.java">src/main/java/org/springframework/samples/petclinic/model/Specialty.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_springdatajpa_SpringDataPetRepository.java">src/main/java/org/springframework/samples/petclinic/repository/springdatajpa/SpringDataPetRepository.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_service_ClinicServiceJpaTests.java">src/test/java/org/springframework/samples/petclinic/service/ClinicServiceJpaTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_test_java_org_springframework_samples_petclinic_service_ClinicServiceSpringDataJpaTests.java">src/test/java/org/springframework/samples/petclinic/service/ClinicServiceSpringDataJpaTests.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>2</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="b">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_springdatajpa_SpringDataVetRepository.java">src/main/java/org/springframework/samples/petclinic/repository/springdatajpa/SpringDataVetRepository.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
         </tr>
         <tr class="a">
            <td>
               <a href="#f-src_main_java_org_springframework_samples_petclinic_repository_springdatajpa_SpringDataVisitRepository.java">src/main/java/org/springframework/samples/petclinic/repository/springdatajpa/SpringDataVisitRepository.java</a>
            </td>
            <td>0</td>
            <td>0</td>
            <td>1</td>
            <td>0</td>
            <td>0</td>
         </tr>
      </table>
      <hr size="1" width="100%" align="left"/>
      <a name="f-src_main_java_org_springframework_samples_petclinic_PetclinicInitializer.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/PetclinicInitializer.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Import Statements.UnusedImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#UnusedImports">
Avoid unused imports such as 'org.springframework.util.Assert'
</a>
            </td>
            <td>21 - 21</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Import Statements.UnusedImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#UnusedImports">
Avoid unused imports such as 'org.springframework.web.WebApplicationInitializer'
</a>
            </td>
            <td>22 - 22</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>33 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>38 - 38</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>44 - 111</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'servletContext' is not assigned and could be declared final
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>58 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>64 - 69</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'rootAppContext' could be declared final
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>67 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>73 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'webAppContext' could be declared final
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>80 - 82</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>85 - 96</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like characterEncodingFilter
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'characterEncodingFilter' could be declared final
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'dandelionFilter' could be declared final
</a>
            </td>
            <td>90 - 90</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'datatablesFilter' could be declared final
</a>
            </td>
            <td>93 - 93</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'filter' is not assigned and could be declared final
</a>
            </td>
            <td>99 - 99</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'servletContext' is not assigned and could be declared final
</a>
            </td>
            <td>99 - 99</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>99 - 103</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'registration' could be declared final
</a>
            </td>
            <td>100 - 100</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>101 - 101</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'servletContext' is not assigned and could be declared final
</a>
            </td>
            <td>105 - 105</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'dandelionServlet' could be declared final
</a>
            </td>
            <td>106 - 106</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'registration' could be declared final
</a>
            </td>
            <td>107 - 107</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>108 - 108</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>109 - 109</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_BaseEntity.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/BaseEntity.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>24 - 24</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>30 - 47</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>35 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'id' is not assigned and could be declared final
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>39 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>43 - 45</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_NamedEntity.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/NamedEntity.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>23 - 23</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>30 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>35 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'name' is not assigned and could be declared final
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>39 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>44 - 46</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_Owner.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/Owner.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>46 - 153</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>61 - 61</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>61 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>64 - 66</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'address' is not assigned and could be declared final
</a>
            </td>
            <td>68 - 68</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>68 - 70</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>72 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'city' is not assigned and could be declared final
</a>
            </td>
            <td>76 - 76</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>76 - 78</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>80 - 82</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'telephone' is not assigned and could be declared final
</a>
            </td>
            <td>84 - 84</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>84 - 86</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>88 - 93</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pets' is not assigned and could be declared final
</a>
            </td>
            <td>95 - 95</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>95 - 97</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>99 - 103</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'sortedPets' could be declared final
</a>
            </td>
            <td>100 - 100</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pet' is not assigned and could be declared final
</a>
            </td>
            <td>105 - 105</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>105 - 108</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 106</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'name' is not assigned and could be declared final
</a>
            </td>
            <td>116 - 116</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p2">2</div>
            </td>
            <td>
						[Design.AvoidReassigningParameters]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#AvoidReassigningParameters">
Avoid reassigning parameters such as 'name'
</a>
            </td>
            <td>126 - 126</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'UR'-anomaly for variable 'pet' (lines '126'-'129').
</a>
            </td>
            <td>126 - 129</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'ignoreNew' is not assigned and could be declared final
</a>
            </td>
            <td>126 - 126</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.UseLocaleWithCaseConversions]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#UseLocaleWithCaseConversions">
When doing a String.toLowerCase()/toUpperCase() call, use a Locale
</a>
            </td>
            <td>127 - 127</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>128 - 128</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>129 - 129</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DD'-anomaly for variable 'compName' (lines '130'-'131').
</a>
            </td>
            <td>130 - 131</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>130 - 130</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>131 - 131</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.UseLocaleWithCaseConversions]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#UseLocaleWithCaseConversions">
When doing a String.toLowerCase()/toUpperCase() call, use a Locale
</a>
            </td>
            <td>131 - 131</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>132 - 132</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>133 - 133</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>141 - 152</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_Person.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/Person.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>29 - 56</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>39 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'firstName' is not assigned and could be declared final
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>43 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>47 - 49</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'lastName' is not assigned and could be declared final
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>51 - 53</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_Pet.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/Pet.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Import Statements.UnusedImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#UnusedImports">
Avoid unused imports such as 'org.joda.time.DateTime'
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Naming.ShortClassName]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortClassName">
Avoid short class names like Pet
</a>
            </td>
            <td>49 - 113</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>49 - 113</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>54 - 54</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>67 - 69</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'birthDate' is not assigned and could be declared final
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>71 - 73</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>75 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'type' is not assigned and could be declared final
</a>
            </td>
            <td>79 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>79 - 81</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>83 - 85</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>87 - 89</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>91 - 96</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visits' is not assigned and could be declared final
</a>
            </td>
            <td>98 - 98</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>98 - 100</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>102 - 106</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'sortedVisits' could be declared final
</a>
            </td>
            <td>103 - 103</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visit' is not assigned and could be declared final
</a>
            </td>
            <td>108 - 108</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>108 - 111</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>109 - 109</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_PetType.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/PetType.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>27 - 29</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_Specialty.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/Specialty.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>28 - 30</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_Vet.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/Vet.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Naming.ShortClassName]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortClassName">
Avoid short class names like Vet
</a>
            </td>
            <td>45 - 78</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>45 - 78</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>52 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'specialties' is not assigned and could be declared final
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>59 - 61</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>64 - 68</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'sortedSpecs' could be declared final
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>70 - 72</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'specialty' is not assigned and could be declared final
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>74 - 76</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>75 - 75</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_Vets.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/Vets.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>25 - 25</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Naming.ShortClassName]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortClassName">
Avoid short class names like Vets
</a>
            </td>
            <td>31 - 43</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>31 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.AvoidFieldNameMatchingTypeName]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#AvoidFieldNameMatchingTypeName">
It is somewhat confusing to have a field name matching the declaring class name
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>36 - 41</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_model_Visit.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/model/Visit.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.CallSuperInConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#CallSuperInConstructor">
It is a good practice to call super() in a constructor
</a>
            </td>
            <td>64 - 66</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'date' is not assigned and could be declared final
</a>
            </td>
            <td>83 - 83</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'description' is not assigned and could be declared final
</a>
            </td>
            <td>101 - 101</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pet' is not assigned and could be declared final
</a>
            </td>
            <td>119 - 119</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_OwnerRepository.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/OwnerRepository.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>24 - 32</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>25 - 25</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>26 - 26</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>52 - 52</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_PetRepository.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/PetRepository.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>25 - 33</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>26 - 26</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>27 - 27</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>50 - 50</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_VetRepository.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/VetRepository.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>23 - 31</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>24 - 24</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>25 - 25</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_VisitRepository.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/VisitRepository.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>24 - 32</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>25 - 25</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>26 - 26</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>43 - 43</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcOwnerRepositoryImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcOwnerRepositoryImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>41 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like namedParameterJdbcTemplate
</a>
            </td>
            <td>54 - 54</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>54 - 54</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'namedParameterJdbcTemplate' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>54 - 54</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>54 - 54</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'insertOwner' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like namedParameterJdbcTemplate
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Unused Code.UnusedFormalParameter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/unusedcode.html#UnusedFormalParameter">
Avoid unused constructor parameters such as 'namedParameterJdbcTemplate'.
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'dataSource' is not assigned and could be declared final
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'namedParameterJdbcTemplate' is not assigned and could be declared final
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>59 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'lastName' is not assigned and could be declared final
</a>
            </td>
            <td>76 - 76</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.UseConcurrentHashMap]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#UseConcurrentHashMap">
If you run in Java5 or newer and have concurrent access, you should use the ConcurrentHashMap implementation
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'params' could be declared final
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'owners' could be declared final
</a>
            </td>
            <td>79 - 83</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>89 - 89</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>93 - 93</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'id' is not assigned and could be declared final
</a>
            </td>
            <td>93 - 93</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.UseConcurrentHashMap]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#UseConcurrentHashMap">
If you run in Java5 or newer and have concurrent access, you should use the ConcurrentHashMap implementation
</a>
            </td>
            <td>96 - 96</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'params' could be declared final
</a>
            </td>
            <td>96 - 96</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>97 - 97</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DU'-anomaly for variable 'owner' (lines '98'-'108').
</a>
            </td>
            <td>98 - 108</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.PreserveStackTrace]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#PreserveStackTrace">
New exception is thrown in catch block, original stack trace may be lost
</a>
            </td>
            <td>104 - 104</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'UR'-anomaly for variable 'pet' (lines '110'-'120').
</a>
            </td>
            <td>110 - 120</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>110 - 123</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.UseConcurrentHashMap]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#UseConcurrentHashMap">
If you run in Java5 or newer and have concurrent access, you should use the ConcurrentHashMap implementation
</a>
            </td>
            <td>111 - 111</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'params' could be declared final
</a>
            </td>
            <td>111 - 111</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DU'-anomaly for variable 'petTypes' (lines '118'-'123').
</a>
            </td>
            <td>118 - 123</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'petTypes' could be declared final
</a>
            </td>
            <td>118 - 118</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>119 - 119</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>126 - 126</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>126 - 137</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'parameterSource' could be declared final
</a>
            </td>
            <td>127 - 127</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'newKey' could be declared final
</a>
            </td>
            <td>129 - 129</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>130 - 130</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>139 - 143</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>146 - 146</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>148 - 148</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'UR'-anomaly for variable 'owner' (lines '151'-'153').
</a>
            </td>
            <td>151 - 153</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owners' is not assigned and could be declared final
</a>
            </td>
            <td>151 - 151</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'owner' could be declared final
</a>
            </td>
            <td>152 - 152</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcPet.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcPet.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>21 - 21</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>26 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>28 - 28</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>30 - 30</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>32 - 34</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'typeId' is not assigned and could be declared final
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>36 - 38</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>40 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'ownerId' is not assigned and could be declared final
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>44 - 46</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcPetRepositoryImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcPetRepositoryImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Import Statements.UnusedImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#UnusedImports">
Avoid unused imports such as 'org.springframework.samples.petclinic.model.Visit'
</a>
            </td>
            <td>35 - 35</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like namedParameterJdbcTemplate
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'namedParameterJdbcTemplate' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'insertPet' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'ownerRepository' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Unused Code.UnusedPrivateField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/unusedcode.html#UnusedPrivateField">
Avoid unused private fields such as 'visitRepository'.
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.SingularField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#SingularField">
Perhaps 'visitRepository' could be replaced by a local variable.
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'visitRepository' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'dataSource' is not assigned and could be declared final
</a>
            </td>
            <td>63 - 63</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'ownerRepository' is not assigned and could be declared final
</a>
            </td>
            <td>63 - 63</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visitRepository' is not assigned and could be declared final
</a>
            </td>
            <td>63 - 63</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>63 - 72</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>75 - 81</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.UseConcurrentHashMap]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#UseConcurrentHashMap">
If you run in Java5 or newer and have concurrent access, you should use the ConcurrentHashMap implementation
</a>
            </td>
            <td>76 - 76</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'params' could be declared final
</a>
            </td>
            <td>76 - 76</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>84 - 84</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'id' is not assigned and could be declared final
</a>
            </td>
            <td>84 - 84</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>84 - 95</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.UseConcurrentHashMap]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#UseConcurrentHashMap">
If you run in Java5 or newer and have concurrent access, you should use the ConcurrentHashMap implementation
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'params' could be declared final
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>88 - 88</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DU'-anomaly for variable 'ownerId' (lines '89'-'95').
</a>
            </td>
            <td>89 - 95</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.PreserveStackTrace]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#PreserveStackTrace">
New exception is thrown in catch block, original stack trace may be lost
</a>
            </td>
            <td>91 - 91</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'owner' could be declared final
</a>
            </td>
            <td>93 - 93</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>94 - 94</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pet' is not assigned and could be declared final
</a>
            </td>
            <td>98 - 98</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>98 - 109</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'newKey' could be declared final
</a>
            </td>
            <td>100 - 101</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>102 - 102</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>112 - 112</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pet' is not assigned and could be declared final
</a>
            </td>
            <td>114 - 114</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>118 - 118</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>119 - 119</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>120 - 120</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcPetRowMapper.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcPetRowMapper.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Import Statements.UnusedImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#UnusedImports">
Avoid unused imports such as 'org.joda.time.DateTime'
</a>
            </td>
            <td>22 - 22</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>27 - 27</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>30 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like rs
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'rownum' is not assigned and could be declared final
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'rs' is not assigned and could be declared final
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>33 - 42</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'birthDate' could be declared final
</a>
            </td>
            <td>37 - 37</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcPetVisitExtractor.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcPetVisitExtractor.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>32 - 34</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like rs
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'rs' is not assigned and could be declared final
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>37 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like rs
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'rs' is not assigned and could be declared final
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>42 - 48</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'child' is not assigned and could be declared final
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'root' is not assigned and could be declared final
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
protectedMethodCommentRequirement Required
</a>
            </td>
            <td>51 - 53</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcVetRepositoryImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcVetRepositoryImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>34 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'jdbcTemplate' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'jdbcTemplate' is not assigned and could be declared final
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>51 - 53</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'UR'-anomaly for variable 'vet' (lines '59'-'73').
</a>
            </td>
            <td>59 - 73</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'UR'-anomaly for variable 'vet' (lines '59'-'73').
</a>
            </td>
            <td>59 - 73</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'vets' could be declared final
</a>
            </td>
            <td>60 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 64</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 64</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DU'-anomaly for variable 'specialties' (lines '67'-'88').
</a>
            </td>
            <td>67 - 88</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DU'-anomaly for variable 'specialties' (lines '67'-'88').
</a>
            </td>
            <td>67 - 88</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'vet' could be declared final
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DU'-anomaly for variable 'vetSpecialtiesIds' (lines '73'-'88').
</a>
            </td>
            <td>73 - 88</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DU'-anomaly for variable 'vetSpecialtiesIds' (lines '73'-'88').
</a>
            </td>
            <td>73 - 88</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.AvoidInstantiatingObjectsInLoops]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#AvoidInstantiatingObjectsInLoops">
Avoid instantiating new objects inside loops
</a>
            </td>
            <td>75 - 80</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like rs
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'row' is not assigned and could be declared final
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'rs' is not assigned and could be declared final
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>77 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'specialtyId' could be declared final
</a>
            </td>
            <td>82 - 82</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'specialty' could be declared final
</a>
            </td>
            <td>83 - 83</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcVisitRepositoryImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcVisitRepositoryImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Import Statements.UnusedImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#UnusedImports">
Avoid unused imports such as 'org.springframework.jdbc.core.JdbcTemplate'
</a>
            </td>
            <td>20 - 20</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>34 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'jdbcTemplate' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'insertVisit' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'dataSource' is not assigned and could be declared final
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>53 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visit' is not assigned and could be declared final
</a>
            </td>
            <td>63 - 63</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>63 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'newKey' could be declared final
</a>
            </td>
            <td>65 - 66</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>67 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>75 - 75</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visit' is not assigned and could be declared final
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>80 - 80</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>82 - 82</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'UR'-anomaly for variable 'visit' (lines '86'-'99').
</a>
            </td>
            <td>86 - 99</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petId' is not assigned and could be declared final
</a>
            </td>
            <td>86 - 86</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>86 - 103</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.UseConcurrentHashMap]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#UseConcurrentHashMap">
If you run in Java5 or newer and have concurrent access, you should use the ConcurrentHashMap implementation
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'params' could be declared final
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'DU'-anomaly for variable 'pet' (lines '89'-'103').
</a>
            </td>
            <td>89 - 103</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>89 - 92</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'visits' could be declared final
</a>
            </td>
            <td>94 - 96</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'visit' could be declared final
</a>
            </td>
            <td>98 - 98</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jdbc_JdbcVisitRowMapper.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jdbc/JdbcVisitRowMapper.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>28 - 28</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>31 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like rs
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'row' is not assigned and could be declared final
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'rs' is not assigned and could be declared final
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>34 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'visit' could be declared final
</a>
            </td>
            <td>35 - 35</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'visitDate' could be declared final
</a>
            </td>
            <td>37 - 37</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jpa_JpaOwnerRepositoryImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jpa/JpaOwnerRepositoryImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>29 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>39 - 81</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like em
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'lastName' is not assigned and could be declared final
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'query' could be declared final
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'id' is not assigned and could be declared final
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>62 - 68</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>64 - 64</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'query' could be declared final
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>66 - 66</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>67 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>72 - 79</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jpa_JpaPetRepositoryImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jpa/JpaPetRepositoryImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>28 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>38 - 63</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like em
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>45 - 47</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'id' is not assigned and could be declared final
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>50 - 52</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pet' is not assigned and could be declared final
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>55 - 61</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jpa_JpaVetRepositoryImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jpa/JpaVetRepositoryImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>28 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>38 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like em
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>47 - 49</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_jpa_JpaVisitRepositoryImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/jpa/JpaVisitRepositoryImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>28 - 38</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>40 - 64</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like em
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visit' is not assigned and could be declared final
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>47 - 53</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petId' is not assigned and could be declared final
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>58 - 62</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'query' could be declared final
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>60 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>61 - 61</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_springdatajpa_SpringDataOwnerRepository.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/springdatajpa/SpringDataOwnerRepository.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Unused Code.UnusedModifier]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/unusedcode.html#UnusedModifier">
Avoid modifiers which are implied by the context
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Unused Code.UnusedModifier]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/unusedcode.html#UnusedModifier">
Avoid modifiers which are implied by the context
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>40 - 40</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_springdatajpa_SpringDataPetRepository.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/springdatajpa/SpringDataPetRepository.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>37 - 37</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_springdatajpa_SpringDataVetRepository.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/springdatajpa/SpringDataVetRepository.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_repository_springdatajpa_SpringDataVisitRepository.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/repository/springdatajpa/SpringDataVisitRepository.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_service_ClinicService.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/service/ClinicService.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>35 - 35</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>51 - 51</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_service_ClinicServiceImpl.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/service/ClinicServiceImpl.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'petRepository' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'vetRepository' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'ownerRepository' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.ImmutableField]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#ImmutableField">
Private field 'visitRepository' could be made final; it is only initialized in the declaration or constructor.
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'ownerRepository' is not assigned and could be declared final
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petRepository' is not assigned and could be declared final
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'vetRepository' is not assigned and could be declared final
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visitRepository' is not assigned and could be declared final
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>50 - 55</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>59 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'id' is not assigned and could be declared final
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>65 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'lastName' is not assigned and could be declared final
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>71 - 73</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>77 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visit' is not assigned and could be declared final
</a>
            </td>
            <td>84 - 84</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>84 - 86</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like id
</a>
            </td>
            <td>91 - 91</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'id' is not assigned and could be declared final
</a>
            </td>
            <td>91 - 91</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>91 - 93</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pet' is not assigned and could be declared final
</a>
            </td>
            <td>97 - 97</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>97 - 99</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>104 - 106</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petId' is not assigned and could be declared final
</a>
            </td>
            <td>109 - 109</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>109 - 111</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_util_CallMonitoringAspect.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/util/CallMonitoringAspect.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>26 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>27 - 27</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>30 - 30</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>39 - 97</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.RedundantFieldInitializer]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#RedundantFieldInitializer">
Avoid using redundant field initializer for 'callCount'
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like accumulatedCallTime
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.RedundantFieldInitializer]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#RedundantFieldInitializer">
Avoid using redundant field initializer for 'accumulatedCallTime'
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>48 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'enabled' is not assigned and could be declared final
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>53 - 55</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>58 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>64 - 66</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>69 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Braces.IfElseStmtsMustUseBraces]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-javascript/rules/ecmascript/braces.html#IfElseStmtsMustUseBraces">
Avoid using if...else statements without curly braces
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Braces.IfElseStmtsMustUseBraces]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-javascript/rules/ecmascript/braces.html#IfElseStmtsMustUseBraces">
Avoid using if...else statements without curly braces
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'joinPoint' is not assigned and could be declared final
</a>
            </td>
            <td>78 - 78</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>78 - 95</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.ShortVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#ShortVariable">
Avoid variables with short names like sw
</a>
            </td>
            <td>80 - 80</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'sw' could be declared final
</a>
            </td>
            <td>80 - 80</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>84 - 84</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_util_EntityUtils.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/util/EntityUtils.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>24 - 32</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>25 - 25</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.AbstractNaming]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#AbstractNaming">
Abstract classes should be named AbstractXXX
</a>
            </td>
            <td>33 - 54</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.AbstractClassWithoutAbstractMethod]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#AbstractClassWithoutAbstractMethod">
This abstract class does not have any abstract methods
</a>
            </td>
            <td>33 - 54</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>35 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'UR'-anomaly for variable 'entity' (lines '44'-'47').
</a>
            </td>
            <td>44 - 47</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'entities' is not assigned and could be declared final
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'entityClass' is not assigned and could be declared final
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'entityId' is not assigned and could be declared final
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'entity' could be declared final
</a>
            </td>
            <td>46 - 46</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_web_CrashController.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/web/CrashController.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>27 - 27</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>31 - 40</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>34 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p1">1</div>
            </td>
            <td>
						[Strict Exceptions.AvoidThrowingRawExceptionTypes]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strictexception.html#AvoidThrowingRawExceptionTypes">
Avoid throwing raw exception types.
</a>
            </td>
            <td>35 - 35</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_web_OwnerController.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/web/OwnerController.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like VIEWS_OWNER_CREATE_OR_UPDATE_FORM
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'clinicService' is not assigned and could be declared final
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>50 - 52</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'dataBinder' is not assigned and could be declared final
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>55 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>60 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>60 - 64</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'owner' could be declared final
</a>
            </td>
            <td>61 - 61</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>67 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'result' is not assigned and could be declared final
</a>
            </td>
            <td>67 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>67 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>69 - 69</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>77 - 80</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p2">2</div>
            </td>
            <td>
						[Design.AvoidReassigningParameters]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#AvoidReassigningParameters">
Avoid reassigning parameters such as 'owner'
</a>
            </td>
            <td>83 - 83</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>83 - 83</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'result' is not assigned and could be declared final
</a>
            </td>
            <td>83 - 83</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>83 - 105</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'results' could be declared final
</a>
            </td>
            <td>91 - 91</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>92 - 92</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>95 - 95</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AvoidLiteralsInIfCondition]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AvoidLiteralsInIfCondition">
Avoid using Literals in Conditional Statements
</a>
            </td>
            <td>96 - 96</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>96 - 96</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>98 - 98</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>98 - 98</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>99 - 99</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>108 - 108</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'ownerId' is not assigned and could be declared final
</a>
            </td>
            <td>108 - 108</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>108 - 112</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'owner' could be declared final
</a>
            </td>
            <td>109 - 109</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>115 - 115</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'ownerId' is not assigned and could be declared final
</a>
            </td>
            <td>115 - 115</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'result' is not assigned and could be declared final
</a>
            </td>
            <td>115 - 115</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>115 - 123</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>117 - 117</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'ownerId' is not assigned and could be declared final
</a>
            </td>
            <td>132 - 132</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'mav' could be declared final
</a>
            </td>
            <td>133 - 133</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>134 - 134</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>134 - 134</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_web_PetController.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/web/PetController.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like VIEWS_PETS_CREATE_OR_UPDATE_FORM
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'clinicService' is not assigned and could be declared final
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>47 - 49</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>52 - 54</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'ownerId' is not assigned and could be declared final
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>57 - 59</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'dataBinder' is not assigned and could be declared final
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>62 - 64</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "pet" appears 5 times in this file; the first occurrence is on line 66
</a>
            </td>
            <td>66 - 66</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'dataBinder' is not assigned and could be declared final
</a>
            </td>
            <td>67 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>67 - 69</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>72 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>80 - 80</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>80 - 80</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pet' is not assigned and could be declared final
</a>
            </td>
            <td>80 - 80</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'result' is not assigned and could be declared final
</a>
            </td>
            <td>80 - 80</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>80 - 92</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>86 - 86</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>95 - 95</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petId' is not assigned and could be declared final
</a>
            </td>
            <td>95 - 95</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>95 - 99</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>96 - 96</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>102 - 102</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'owner' is not assigned and could be declared final
</a>
            </td>
            <td>102 - 102</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'pet' is not assigned and could be declared final
</a>
            </td>
            <td>102 - 102</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'result' is not assigned and could be declared final
</a>
            </td>
            <td>102 - 102</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>102 - 111</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>105 - 105</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_web_PetTypeFormatter.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/web/PetTypeFormatter.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>28 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>29 - 29</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>30 - 30</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>31 - 31</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>32 - 32</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'clinicService' is not assigned and could be declared final
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>46 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'locale' is not assigned and could be declared final
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petType' is not assigned and could be declared final
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>51 - 53</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p5">5</div>
            </td>
            <td>
						[Controversial.DataflowAnomalyAnalysis]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#DataflowAnomalyAnalysis">
Found 'UR'-anomaly for variable 'type' (lines '56'-'59').
</a>
            </td>
            <td>56 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'locale' is not assigned and could be declared final
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'text' is not assigned and could be declared final
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>56 - 64</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'findPetTypes' could be declared final
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'type' could be declared final
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>59 - 59</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_web_PetValidator.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/web/PetValidator.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>23 - 31</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>26 - 26</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>32 - 65</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'errors' is not assigned and could be declared final
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'obj' is not assigned and could be declared final
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>37 - 54</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>38 - 38</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'name' could be declared final
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'clazz' is not assigned and could be declared final
</a>
            </td>
            <td>60 - 60</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 61</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_web_VetController.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/web/VetController.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'clinicService' is not assigned and could be declared final
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>40 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>45 - 52</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'vets' could be declared final
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>57 - 63</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'vets' could be declared final
</a>
            </td>
            <td>60 - 60</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 61</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 61</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_main_java_org_springframework_samples_petclinic_web_VisitController.java"/>
      <h3>File src/main/java/org/springframework/samples/petclinic/web/VisitController.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'clinicService' is not assigned and could be declared final
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>48 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'dataBinder' is not assigned and could be declared final
</a>
            </td>
            <td>53 - 53</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>53 - 55</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>57 - 66</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>61 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petId' is not assigned and could be declared final
</a>
            </td>
            <td>68 - 68</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>69 - 69</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'visit' could be declared final
</a>
            </td>
            <td>70 - 70</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petId' is not assigned and could be declared final
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>77 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>81 - 81</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'result' is not assigned and could be declared final
</a>
            </td>
            <td>83 - 83</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'visit' is not assigned and could be declared final
</a>
            </td>
            <td>83 - 83</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>83 - 90</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.OnlyOneReturn]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#OnlyOneReturn">
A method should have only one exit point, and that should be the last statement in the method
</a>
            </td>
            <td>85 - 85</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'model' is not assigned and could be declared final
</a>
            </td>
            <td>93 - 93</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.MethodArgumentCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#MethodArgumentCouldBeFinal">
Parameter 'petId' is not assigned and could be declared final
</a>
            </td>
            <td>93 - 93</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>93 - 96</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>94 - 94</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>94 - 94</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>94 - 94</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_model_ValidatorTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/model/ValidatorTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>18 - 18</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>20 - 45</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like localValidatorFactoryBean
</a>
            </td>
            <td>23 - 23</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'localValidatorFactoryBean' could be declared final
</a>
            </td>
            <td>23 - 23</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>29 - 29</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>29 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'person' could be declared final
</a>
            </td>
            <td>32 - 32</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'validator' could be declared final
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like constraintViolations
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'constraintViolations' could be declared final
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'violation' could be declared final
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>42 - 42</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_service_AbstractClinicServiceTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/service/AbstractClinicServiceTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p4">4</div>
            </td>
            <td>
						[Import Statements.UnusedImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#UnusedImports">
Avoid unused imports such as 'org.joda.time.DateTime'
</a>
            </td>
            <td>22 - 22</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>35 - 53</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>38 - 38</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>54 - 204</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.AbstractClassWithoutAbstractMethod]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#AbstractClassWithoutAbstractMethod">
This abstract class does not have any abstract methods
</a>
            </td>
            <td>54 - 204</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Code Size.TooManyMethods]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-plsql/rules/plsql/codesize.html#TooManyMethods">
This class has too many methods, consider refactoring it.
</a>
            </td>
            <td>54 - 204</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>60 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>60 - 66</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>69 - 69</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>69 - 75</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'owner' could be declared final
</a>
            </td>
            <td>70 - 70</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>71 - 71</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>79 - 79</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>79 - 94</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'found' could be declared final
</a>
            </td>
            <td>81 - 81</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>81 - 81</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'owner' could be declared final
</a>
            </td>
            <td>83 - 83</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>84 - 84</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>85 - 85</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>86 - 86</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>88 - 88</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>90 - 90</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>90 - 90</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>90 - 90</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>90 - 90</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>93 - 93</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>93 - 93</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>98 - 109</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'oldLastName' could be declared final
</a>
            </td>
            <td>100 - 100</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>100 - 100</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'newLastName' could be declared final
</a>
            </td>
            <td>101 - 101</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>103 - 103</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>108 - 108</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>108 - 108</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>112 - 112</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>112 - 117</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet7' could be declared final
</a>
            </td>
            <td>113 - 113</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>114 - 114</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>114 - 114</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>115 - 115</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>115 - 115</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>115 - 115</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>115 - 115</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>120 - 120</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>120 - 127</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'petTypes' could be declared final
</a>
            </td>
            <td>121 - 121</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'petType1' could be declared final
</a>
            </td>
            <td>123 - 123</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>124 - 124</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>124 - 124</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'petType4' could be declared final
</a>
            </td>
            <td>125 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>126 - 126</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>126 - 126</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>131 - 131</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>131 - 150</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'found' could be declared final
</a>
            </td>
            <td>133 - 133</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>133 - 133</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>133 - 133</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'pet' could be declared final
</a>
            </td>
            <td>135 - 135</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>136 - 136</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'types' could be declared final
</a>
            </td>
            <td>137 - 137</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>138 - 138</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>140 - 140</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>141 - 141</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>141 - 141</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>141 - 141</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>141 - 141</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>147 - 147</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>147 - 147</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>147 - 147</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>147 - 147</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>149 - 149</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>149 - 149</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Strict Exceptions.SignatureDeclareThrowsException]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strictexception.html#SignatureDeclareThrowsException">
A method/constructor shouldnt explicitly throw java.lang.Exception
</a>
            </td>
            <td>154 - 154</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>154 - 164</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'oldName' could be declared final
</a>
            </td>
            <td>156 - 156</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>156 - 156</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'newName' could be declared final
</a>
            </td>
            <td>158 - 158</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>159 - 159</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>163 - 163</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>163 - 163</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>167 - 167</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>167 - 175</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'vets' could be declared final
</a>
            </td>
            <td>168 - 168</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'vet' could be declared final
</a>
            </td>
            <td>170 - 170</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>171 - 171</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>171 - 171</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>172 - 172</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>172 - 172</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>173 - 173</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>173 - 173</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>173 - 173</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>173 - 173</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>173 - 173</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>173 - 173</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>174 - 174</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>174 - 174</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>174 - 174</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>174 - 174</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>174 - 174</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>174 - 174</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>179 - 179</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>179 - 191</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'found' could be declared final
</a>
            </td>
            <td>181 - 181</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>181 - 181</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>181 - 181</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'visit' could be declared final
</a>
            </td>
            <td>182 - 182</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>183 - 183</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>184 - 184</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>189 - 189</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>189 - 189</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>189 - 189</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>189 - 189</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>190 - 190</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>190 - 190</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Strict Exceptions.SignatureDeclareThrowsException]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strictexception.html#SignatureDeclareThrowsException">
A method/constructor shouldnt explicitly throw java.lang.Exception
</a>
            </td>
            <td>194 - 194</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitTestContainsTooManyAsserts]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitTestContainsTooManyAsserts">
JUnit tests should not contain more than 1 assert(s).
</a>
            </td>
            <td>194 - 194</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>194 - 201</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'visits' could be declared final
</a>
            </td>
            <td>195 - 195</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>196 - 196</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>196 - 196</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'visitArr' could be declared final
</a>
            </td>
            <td>197 - 197</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>197 - 197</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>198 - 198</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>198 - 198</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>198 - 198</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>198 - 198</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>198 - 198</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>199 - 199</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>199 - 199</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>199 - 199</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>199 - 199</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>199 - 199</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>200 - 200</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>200 - 200</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>200 - 200</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>200 - 200</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>200 - 200</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>200 - 200</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>200 - 200</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_service_ClinicServiceJdbcTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/service/ClinicServiceJdbcTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Too many lines
</a>
            </td>
            <td>1 - 15</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>28 - 28</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>33 - 36</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_service_ClinicServiceJpaTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/service/ClinicServiceJpaTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>14 - 14</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>20 - 22</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_service_ClinicServiceSpringDataJpaTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/service/ClinicServiceSpringDataJpaTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentSize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentSize">
Comment is too large: Line too long
</a>
            </td>
            <td>12 - 12</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>18 - 20</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_web_CrashControllerTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/web/CrashControllerTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>27 - 53</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>30 - 30</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>30 - 30</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like simpleMappingExceptionResolver
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>35 - 35</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>35 - 35</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitSpelling]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitSpelling">
You may have misspelled a JUnit framework method (setUp or tearDown)
</a>
            </td>
            <td>38 - 38</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>38 - 43</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>39 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>39 - 42</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>46 - 52</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>47 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>47 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>47 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>47 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>47 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>47 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>47 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>48 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>51 - 51</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_web_OwnerControllerTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/web/OwnerControllerTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Import Statements.TooManyStaticImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#TooManyStaticImports">
Too many static imports may lead to messy code
</a>
            </td>
            <td>1 - 167</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>32 - 167</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Code Size.TooManyMethods]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-plsql/rules/plsql/codesize.html#TooManyMethods">
This class has too many methods, consider refactoring it.
</a>
            </td>
            <td>32 - 167</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitSpelling]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitSpelling">
You may have misspelled a JUnit framework method (setUp or tearDown)
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>42 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>47 - 52</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>48 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>48 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>48 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>48 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>48 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>48 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "owner" appears 21 times in this file; the first occurrence is on line 50
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "owners/createOrUpdateOwnerForm" appears 4 times in this file; the first occurrence is on line 51
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>55 - 64</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 61</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 61</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 61</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 63</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 63</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 63</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 63</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 63</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 63</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 63</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "Joe" appears 4 times in this file; the first occurrence is on line 57
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "firstName" appears 6 times in this file; the first occurrence is on line 57
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "Bloggs" appears 4 times in this file; the first occurrence is on line 58
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "lastName" appears 10 times in this file; the first occurrence is on line 58
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "address" appears 6 times in this file; the first occurrence is on line 59
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "London" appears 4 times in this file; the first occurrence is on line 60
</a>
            </td>
            <td>60 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "city" appears 6 times in this file; the first occurrence is on line 60
</a>
            </td>
            <td>60 - 60</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "telephone" appears 6 times in this file; the first occurrence is on line 61
</a>
            </td>
            <td>61 - 61</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>63 - 63</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>67 - 78</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 71</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 71</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>68 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>75 - 75</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>76 - 76</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>81 - 86</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>82 - 85</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>82 - 85</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>82 - 85</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>82 - 85</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>82 - 85</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>82 - 85</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>83 - 83</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>84 - 84</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>85 - 85</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>89 - 93</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>90 - 92</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>90 - 92</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>90 - 92</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>90 - 92</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>91 - 91</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 92</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>96 - 102</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>97 - 98</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>97 - 101</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>97 - 101</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>97 - 101</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>97 - 101</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>97 - 101</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>100 - 100</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>101 - 101</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>105 - 113</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 107</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>106 - 112</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>109 - 109</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>110 - 110</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>111 - 111</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>112 - 112</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>116 - 126</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>117 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>118 - 118</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>119 - 119</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>120 - 120</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>121 - 121</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>122 - 122</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>123 - 123</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>124 - 124</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>125 - 125</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>129 - 139</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 135</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 135</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 135</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 135</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 135</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>130 - 138</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>137 - 137</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>138 - 138</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>142 - 153</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 146</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 146</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 146</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>143 - 152</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>148 - 148</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>149 - 149</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>150 - 150</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>151 - 151</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>152 - 152</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>156 - 165</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>157 - 164</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>158 - 158</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>159 - 159</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>160 - 160</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>161 - 161</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>162 - 162</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>163 - 163</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>164 - 164</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_web_PetControllerTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/web/PetControllerTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Import Statements.TooManyStaticImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#TooManyStaticImports">
Too many static imports may lead to messy code
</a>
            </td>
            <td>1 - 113</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>30 - 113</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>32 - 32</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>33 - 33</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Naming.LongVariable]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/naming.html#LongVariable">
Avoid excessively long variable names like formattingConversionServiceFactoryBean
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>41 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitSpelling]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitSpelling">
You may have misspelled a JUnit framework method (setUp or tearDown)
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>44 - 49</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>45 - 48</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>45 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>52 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>53 - 56</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>53 - 56</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>53 - 56</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>53 - 56</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>53 - 56</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>53 - 56</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>54 - 54</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "pets/createOrUpdatePetForm" appears 4 times in this file; the first occurrence is on line 55
</a>
            </td>
            <td>55 - 55</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "pet" appears 4 times in this file; the first occurrence is on line 56
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>60 - 68</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 64</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 64</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 64</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>61 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "Betty" appears 4 times in this file; the first occurrence is on line 62
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "name" appears 4 times in this file; the first occurrence is on line 62
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "2015/02/12" appears 4 times in this file; the first occurrence is on line 64
</a>
            </td>
            <td>64 - 64</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "birthDate" appears 4 times in this file; the first occurrence is on line 64
</a>
            </td>
            <td>64 - 64</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>66 - 66</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>67 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>71 - 80</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 74</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 79</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[String and StringBuffer.AvoidDuplicateLiterals]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/strings.html#AvoidDuplicateLiterals">
The String literal "/owners/{ownerId}/pets/{petId}/edit" appears 4 times in this file; the first occurrence is on line 72
</a>
            </td>
            <td>72 - 72</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>76 - 76</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>77 - 77</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>78 - 78</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>79 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>83 - 88</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>84 - 87</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>84 - 87</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>84 - 87</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>84 - 87</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>84 - 87</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>84 - 87</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>85 - 85</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>86 - 86</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>87 - 87</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>91 - 99</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 95</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 95</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 95</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 98</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 98</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 98</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 98</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 98</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 98</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>92 - 98</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>97 - 97</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>98 - 98</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>102 - 111</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 105</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 105</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>103 - 110</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>107 - 107</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>108 - 108</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>109 - 109</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>110 - 110</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_web_PetTypeFormatterTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/web/PetTypeFormatterTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>23 - 76</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>26 - 26</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>26 - 26</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>28 - 28</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>28 - 28</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitSpelling]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitSpelling">
You may have misspelled a JUnit framework method (setUp or tearDown)
</a>
            </td>
            <td>31 - 31</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>31 - 33</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>36 - 41</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'petType' could be declared final
</a>
            </td>
            <td>37 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'petTypeName' could be declared final
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>44 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>45 - 45</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'petType' could be declared final
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>51 - 54</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 52</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'petTypes' could be declared final
</a>
            </td>
            <td>62 - 62</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.NonStaticInitializer]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#NonStaticInitializer">
Non-static initializers are confusing
</a>
            </td>
            <td>64 - 66</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Design.NonStaticInitializer]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/design.html#NonStaticInitializer">
Non-static initializers are confusing
</a>
            </td>
            <td>69 - 71</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_web_VetControllerTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/web/VetControllerTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>27 - 63</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>30 - 30</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>30 - 30</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>32 - 32</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>32 - 32</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitSpelling]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitSpelling">
You may have misspelled a JUnit framework method (setUp or tearDown)
</a>
            </td>
            <td>35 - 35</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>35 - 37</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>40 - 45</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>41 - 44</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>42 - 42</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>43 - 43</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>44 - 44</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>48 - 53</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Optimization.LocalVariableCouldBeFinal]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/optimizations.html#LocalVariableCouldBeFinal">
Local variable 'actions' could be declared final
</a>
            </td>
            <td>49 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 49</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>49 - 50</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>50 - 50</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>51 - 51</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>51 - 52</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>51 - 52</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>51 - 52</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (object not created locally)
</a>
            </td>
            <td>51 - 52</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 52</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>56 - 61</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 60</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 60</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 60</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 60</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>58 - 58</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>59 - 59</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>60 - 60</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <a name="f-src_test_java_org_springframework_samples_petclinic_web_VisitControllerTests.java"/>
      <h3>File src/test/java/org/springframework/samples/petclinic/web/VisitControllerTests.java</h3>
      <table class="log" border="0" cellpadding="5" cellspacing="2" width="100%">
         <tr>
            <th style="width:60px;">Violation</th>
            <th>Error Description</th>
            <th style="width:40px;">Line</th>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Import Statements.TooManyStaticImports]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/imports.html#TooManyStaticImports">
Too many static imports may lead to messy code
</a>
            </td>
            <td>1 - 79</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Controversial.AtLeastOneConstructor]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/controversial.html#AtLeastOneConstructor">
Each class should declare at least one constructor
</a>
            </td>
            <td>29 - 79</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>31 - 31</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>34 - 34</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JavaBeans.BeanMembersShouldSerialize]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/javabeans.html#BeanMembersShouldSerialize">
Found non-transient, non-static member. Please mark as transient or provide accessors.
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
fieldCommentRequirement Required
</a>
            </td>
            <td>36 - 36</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[JUnit.JUnitSpelling]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/junit.html#JUnitSpelling">
You may have misspelled a JUnit framework method (setUp or tearDown)
</a>
            </td>
            <td>39 - 39</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>39 - 41</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>40 - 40</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>44 - 48</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>45 - 47</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>45 - 47</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>45 - 47</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>45 - 47</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>46 - 46</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>47 - 47</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>51 - 58</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 54</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 54</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 57</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>52 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>56 - 56</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>57 - 57</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>61 - 68</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 63</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>62 - 67</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>65 - 65</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>66 - 66</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>67 - 67</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Comments.CommentRequired]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/comments.html#CommentRequired">
publicMethodCommentRequirement Required
</a>
            </td>
            <td>71 - 76</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 75</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 75</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 75</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 75</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 75</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>72 - 75</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>73 - 73</td>
         </tr>
         <tr class="a">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>74 - 74</td>
         </tr>
         <tr class="b">
            <td>
               <div class="p3">3</div>
            </td>
            <td>
						[Coupling.LawOfDemeter]
						 -
						 <a href="https://pmd.github.io/pmd-5.5.0/pmd-java/rules/java/coupling.html#LawOfDemeter">
Potential violation of Law of Demeter (method chain calls)
</a>
            </td>
            <td>75 - 75</td>
         </tr>
      </table>
      <a href="#top">Back to top</a>
      <hr size="1" width="100%" align="left"/>
   </body>
</html>